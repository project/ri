<?php

namespace Drupal\ri\Commands;

use Drupal;
use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
class RiCommands extends DrushCommands {

  /**
   * Executes the release instructions.
   * @command ri
   */
  public function ri() {
    $updates = $this->updates(FALSE);

    // Now run the updates.
    foreach ($updates as $module => $functions) {
      foreach ($functions as $function => $version) {
        // Skip if already executed.
        if ($this->statusGet($function)) {
          continue;
        }

        $this->executeFunction($function);
      }
    }

    drush_log('Release instructions were executed.', 'ok');
  }

  /**
   * Previews scheduled release instructions.
   * @command ri:preview
   * @aliases rip,ri-preview
   */
  public function preview() {
    $updates = $this->updates(TRUE);

    foreach ($updates as $module => $functions) {
      foreach ($functions as $function => $version) {
        $message = $function;

        // Message.
        drush_log($message, 'status');
      }
    }
  }

  /**
   * Sets status for the release instruction function.
   *
   * @param $function
   *   Function name.
   * @param $flag
   *   Flag (1 or 0).
   *
   * @command ri:status
   * @aliases ris,ri-status
   */
  public function status($function, $flag) {
    $this->statusSet($function, $flag);
    drush_log(sprintf('Status for %s() was set to %s.', $function, $flag ? 1 : 0), 'ok');
  }

  /**
   * Runs a single release instruction.
   *
   * @param $function
   *   Function name.
   *
   * @command ri:function
   * @aliases rif,ri-function
   */
  public function execute($function) {
    // Preload the files.
    $updates = $this->updates(FALSE);

    // Function.
    if (FALSE === strpos($function, '*')) {
      $this->executeFunction($function);
    }

    // Wildcard.
    else {
      $pattern = '@^' . str_replace('*', '.*', $function) . '$@';

      // Now run the updates.
      foreach ($updates as $module => $functions) {
        foreach ($functions as $function => $version) {
          if (preg_match($pattern, $function)) {
            $this->executeFunction($function);
          }
        }
      }
    }
  }

  /**
   * Gets the status of the RI.
   *
   * @param null $function
   *
   * @return bool|mixed
   */
  public function statusGet($function = NULL) {
    $ri_executed = \Drupal::state()->get('ri_executed', array());
    return is_null($function) ? $ri_executed : !empty($ri_executed[$function]);
  }

  /**
   * @param $function
   * @param bool $flag
   */
  public function statusSet($function, $flag = TRUE) {
    $ri_executed = $this->statusGet();
    $ri_executed[$function] = $flag ? 1 : 0;
    Drupal::state()->set('ri_executed', $ri_executed);
  }

  /**
   * @param $function
   */
  private function executeFunction($function) {
    // Delimiter.
    $delimiter = '##############################';
    echo $delimiter, "\n";
    echo $function, '()', "\n";

    // Execute.
    if (function_exists($function)) {
      if (!($message = $function())) {
        $message = 'Release instructions ' . $function . '() were executed.';
      }

      // Mark as executed.
      $this->statusSet($function);
    }
    else {
      $message = 'Release instruction ' . $function . '() does not exist.';
    }

    // Message.
    drush_log($message, 'status');

    // Delimiter.
    echo $delimiter, "\n\n";
  }

  /**
   * @return array
   */
  private function files() {
    $ri_files = array();

    $hook = 'ri';
    foreach (\Drupal::moduleHandler()->getImplementations($hook) as $module) {
      $directory = DRUPAL_ROOT . '/' . drupal_get_path('module', $module) . '/ri';
      if (is_dir($directory)) {
        if ($files = \Drupal::service('file_system')->scanDirectory($directory, '/.*\.ri\.inc$/')) {
          foreach ($files as $file) {
            $ri_files[] = array(
              'module' => $module,
              'name' => $file->name,
            );
          }
        }
      }
    }

    return $ri_files;
  }

  /**
   * Returns the list of all the RIs available.
   *
   * @param bool $exclude_executed
   *
   * @return array
   */
  private function updates($exclude_executed = TRUE) {
    $updates = array();

    if ($ri_files = $this->files()) {
      foreach ($ri_files as $ri_file) {
        // Load file.
        module_load_include('inc', $ri_file['module'], 'ri/' . $ri_file['name']);

        // Get function names prefix.
        $separator = '_';
        $function_name = trim(preg_replace('@[^a-z0-9_]+@', $separator, mb_strtolower($ri_file['name'])), $separator);

        // Prepare regular expression to match all possible defined hook_update_N().
        $regexp = '/^' . $function_name . '_(?P<version>\d+)$/';
        $functions = get_defined_functions();
        // Narrow this down to functions ending with an integer, since all
        // hook_update_N() functions end this way, and there are other
        // possible functions which match '_update_'. We use preg_grep() here
        // instead of foreaching through all defined functions, since the loop
        // through all PHP functions can take significant page execution time
        // and this function is called on every administrative page via
        // system_requirements().
        foreach (preg_grep('/_\d+$/', $functions['user']) as $function) {
          // If this function is a module update function, add it to the list of
          // module updates.
          if (preg_match($regexp, $function, $matches)) {
            $updates[$ri_file['module']][$function] = $matches['version'];
          }
        }
        // Ensure that updates are applied in numerical order.
        foreach ($updates as &$module_updates) {
          ksort($module_updates);
        }
      }
    }

    // Exclude.
    if ($exclude_executed) {
      foreach ($updates as $module => $functions) {
        foreach ($functions as $function => $version) {
          if ($this->statusGet($function)) {
            unset($updates[$module][$function]);
          }
        }
      }
    }

    return $updates;
  }
}
