<?php

/**
 * Implements hook_drush_command().
 */
function ri_drush_command() {
  $items['ri'] = array(
    'description' => 'Executes the release instructions.',
    'aliases' => array(),
  );

  $items['ri-preview'] = array(
    'description' => 'Previews scheduled release instructions.',
    'aliases' => array('rip'),
  );

  $items['ri-status'] = array(
    'description' => 'Sets status for the release instruction function.',
    'aliases' => array('ris'),
    'arguments' => array(
      'function' => 'Function name.',
      'flag' => 'Flag (1 or 0).',
    ),
  );

  $items['ri-function'] = array(
    'description' => 'Runs a single release instruction.',
    'aliases' => array('rif'),
    'arguments' => array(
      'function' => 'Function name.',
    ),
  );

  return $items;
}

/**
 * @return \Drupal\ri\Commands\RiCommands
 */
function ri_get_instance() {
  static $instance;

  if (is_null($instance)) {
    $instance = new \Drupal\ri\Commands\RiCommands();
  }

  return $instance;
}

/**
 * Callback for "ri" command
 */
function drush_ri() {
  ri_get_instance()->ri();
}

/**
 * Callback for "ri-preview" command
 */
function drush_ri_preview() {
  ri_get_instance()->preview();
}

/**
 * Callback for "ri-status" command
 */
function drush_ri_status($function, $flag) {
  ri_get_instance()->status($function, $flag);
}

/**
 * Callback for "ri-function" command
 */
function drush_ri_function($function) {
  ri_get_instance()->execute($function);
}

/**
 * @param null $function
 *
 * @return bool|null
 */
function ri_executed_get($function = NULL) {
  return ri_get_instance()->statusGet($function);
}

/**
 * @param $function
 * @param bool $flag
 */
function ri_executed_set($function, $flag = TRUE) {
  return ri_get_instance()->statusSet($function, $flag);
}
